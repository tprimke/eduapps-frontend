# Projekt nadrzędny

Celem jest napisanie *wielu* niezależnych SPA. Każda SPA będzie dotyczyła innego problemu edukacyjnego (np. BLM, IP).

Docelowo, każda SPA ma działać w taki sam sposób:
  - użytkownik loguje się do aplikacji,
  - użytkownik generuje problem do rozwiązania,
  - użytkownik decyduje, czy rozwiązuje wygenerowany problem w trybie nauki (bez konsekwencji), czy w trybie testowym (decyzje są zapisywane na serwerze),
    W trybie testowym, w każdym momencie użytkownik może przejść w tryb nauki. Zmiana w odwrotnym kierunku nie jest możliwa.
  - użytkownik, rozwiązując problem, podejmuje kolejne decyzje
    - zawsze dostępne są WSZYSTKIE decyzje do podjęcia
      nie wszystkie decyzją są jednak DOPUSZCZALNE w danym momencie rozwiązywania zadania
    - podjęcie decyzji niedopuszczalnej skutkuje
      - stosownym komunikatem
      - ograniczeniem możliwości decyzji do podjęcia do decyzji dopuszczalnych
    - jedną z możliwych decyzji do podjęcia jest zakończenie rozwiązywania
      Oznacza to, że użytkownik uznaje problem za rozwiązany.
    - możliwe jest również podjęcie decyzji o rezygnacji z rozwiązania problemu
      wówczas do końca rozwiązania będą dostępne wyłącznie decyzje dopuszczalne
  - po rozwiązaniu problemu użytkownik może wygenerować kolejny problem

Ponieważ problemy mogą być różne, to aplikacje będą rozwijane jako jeden projekt (nadrzędny). Poszczególne prace dyplomowe będą dotyczyły osobnych SPA, względnie osobnych modułów w poszczególnych SPA.

# Koncepcja interfejsu użytkownika

Ogólny wzorzec może wyglądać [tak](http://codepen.io/kgrzebielucha/pen/akGPvk?editors=1010).

Wszystkie aplikacje będą się składać z następujących komponentów:

<Header />
<Info />
<Summary />
<Diagram />
<Solution />
<Decisions />
<Form />

W nagłówku będą podstawowe informacje dotyczące aplikacji (nazwa, stan, numer rozwiązywanego problemu).

W części informacyjnej:
  - dane użytkownika (albo informacja, że aplikacja działa w trybie offline),
  - statystyki z poprzednio rozwiązanych zadań

W podsumowaniu będą informacje o aktualnej sesji użytkownika. Dane te będą zależne od konkretnego problemu. Np. dla BLM będzie to informacja o tym, jakie rozwiązania problemu zostały już wyznaczone. Dla problemu IP może to być informacja o najlepszym znalezionym dotychczas rozwiązaniu.

W części z grafem będzie wizualizacja aktualnie rozwiązywanego problemu.

W części z rozwiązaniem będą wszelkie informacje dotyczące aktualnie rozwiązywanego problemu. Coś w rodzaju "ściągi" dla użytkownika.

W części z decyzjami będą przyciski odpowiadające decyzjom, jakie może podjąć użytkownik.

Formularz będzie wykorzystywany do wprowadzania danych związanych z decyzją do podjęcia. Każdy formularz będzie miał dwa przyciski: anulowania i podjęcia decyzji. Przycisk podjęcia decyzji powinien być jak najmniejszy.

# Struktura aplikacji

Aplikacja będzie się składała z komponentów React-a (z których każdy będzie osobnym modułem), oraz z paru dodatkowych modułów. Architektura aplikacji jest przedstawiona w pliku 'Architecture.odg'.

## Jak to działa

React służy tylko do tworzenia widoku - a więc samego layoutu komponentów. Każdy komponent składa się z elementów DOM, które prezentują pewne dane. Dane te pochodzą z modelu. Istotne są tutaj trzy koncepcje:
  - model jest jeden, dla całej aplikacji ("the single source of truth"),
  - model może ulegać zmianie tylko w wyniku wykonania pewnych *akcji*,
  - model wysyła powiadomienia do wszystkich zarejestrowanych klientów zawsze wówczas, kiedy stan modelu ulegnie zmianie.

Wynika z tego, że każdy (główny) komponent React powinien "nasłuchiwać" zmian w modelu i odpowiednio na nie reagować.

## Model

Model będzie w istocie modułem, którego zadaniem będzie:
  - rejestracja listenerów (funkcji wywoływanych wówczas, gdy stan modelu ulegnie zmianie),
  - uruchamianie akcji (zapewne na drodze delegacji do innych modułów).

Na chwilę obecną przewiduję następujące moduły:
  
  - Problem: moduł z funkcjami dotyczącymi problemu optymalizacji.
    - struktury danych (obiekty POJO)
    - funkcje działające na tych obiektach (tworzenie problemu, rozwiązywanie problemu, funkcje pomocnicze)

Pozostałe moduły będą przeznaczone do obsługi poszczególnych komponentów aplikacji.

# Jak zacząć pracę?

Upewnij się, że:

  - rozumiesz idee [Thinking in React](https://facebook.github.io/react/docs/thinking-in-react.html)
  - potrafisz korzystać z git-a

Potem:

  1. Sforkuj repozytorium.
  2. Zapoznaj się z plikami w projekcie.
     Upewnij się, że potrafisz zbudować projekt i uruchomić testy.
  3. Dowiedz się, jakie masz zadanie do wykonania.
     Jeśli nie ma testów do tego zadania, upomnij się o nie (albo sam je napisz).
     Jeśli są testy, przystąp do pracy.
     Pracuj tak długo, aż testy będą zaliczone.
  4. Jak już skończysz zadanie, zrób merge request do głównego repo projektu i poczekaj na opinię.

Pytania? Zadaj je na czacie.
