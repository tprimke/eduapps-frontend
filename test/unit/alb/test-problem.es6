var should = require('should'),
    salbp  = require('../../../src/js/alb/problem');

describe('ALB Problem', () => {

  let problem = null,
      solution = null,
      solution2 = null;

  beforeEach( () => {
    problem = {
      operations: [
        { id: 1, time: 1, accs: [2, 3] },
        { id: 2, time: 3, accs: [5, 6] },
        { id: 3, time: 4, accs: [6]    },
        { id: 4, time: 2, accs: [6]    },
        { id: 5, time: 3, accs: [7]    },
        { id: 6, time: 2, accs: [7]    },
        { id: 7, time: 3 }
      ]
    };

    solution = {
      weights: [
        { id: 1, weight: 1 },
        { id: 2, weight: 3 },
        { id: 3, weight: 4 },
        { id: 4, weight: 2 },
        { id: 5, weight: 3 },
        { id: 6, weight: 2 },
        { id: 7, weight: 3 }
      ],

      stations: [
        { id: 1, time: 3,
          operations: [
            { id: 4, start: 0 },
            { id: 1, start: 2 }
          ] },
        { id: 2, time: 4,
          operations: [
            { id: 3, start: 0 }
          ] }
      ]
    };

    solution2 = {
      weights: [
        { id: 1, weight: 1 },
        { id: 2, weight: 3 },
        { id: 3, weight: 4 },
        { id: 4, weight: 2 },
        { id: 5, weight: 3 },
        { id: 6, weight: 2 },
        { id: 7, weight: 3 }
      ],

      stations: [
        { id: 1, time: 3,
          operations: [
            { id: 4, start: 0 },
            { id: 1, start: 2 }
          ] }
      ]
    };
  });

  let checkSolutionFrozen = (sol) => {
    Object.isFrozen(sol).should.be.true();
    Object.isFrozen(sol.stations).should.be.true();
    sol.stations.forEach( (s) => {
      Object.isFrozen(s.operations).should.be.true();
    });
  };

  it('emptySolution', () => {
    let sol = salbp.emptySolution(problem);

    // sol = {
    //   stations: [
    //     { id: 1, time: 0, operations: [] }
    //   ]
    // }

    sol.should.deepEqual({
      stations: [
        { id: 1, time: 0, operations: [] }
      ]
    });
    
    // all should be frozen
    checkSolutionFrozen(sol);
  });

  it('opTime', () => {
    salbp.opTime(problem, 1).should.eql(1);
    salbp.opTime(problem, 3).should.eql(4);
    salbp.opTime(problem, 6).should.eql(2);
  });

  let checkArray = (actual, expected) => {
    actual.should.be.an.Array;
    expected.should.be.an.Array;
    actual.length.should.eql(expected.length);

    actual.forEach( (x) => {
      (expected.indexOf(x) !== -1).should.be.true();
    });
  };

  it('opAccs', () => {
    salbp.accs(problem, 1).should.deepEqual( [2, 3] );
    salbp.accs(problem, 4).should.deepEqual( [6] );
    salbp.accs(problem, 7).should.deepEqual( [] );
  });

  it('opPreds', () => {
    salbp.preds(problem, 1).should.deepEqual( [] );
    salbp.preds(problem, 6).should.deepEqual( [3, 4] );
    salbp.preds(problem, 5).should.deepEqual( [2] );
  });

  describe( 'weights', () => {

    let checkWeights = (method, expected) => {
      let w = salbp.weights(problem, method);

      w.should.deepEqual(expected);
    };

    it('WET', () => {
      checkWeights('wet', [1, 3, 4, 2, 1, 2, 3]);
    });

    it('RPW', () => {
      checkWeights('rpw', [10, 8, 9, 7, 4, 5, 3]);
    });

  });

  it('opWeight', () => {
    salbp.opWeight(solution, 1).should.eql(1);
    salbp.opWeight(solution, 3).should.eql(4);
    salbp.opWeight(solution, 5).should.eql(1);
  });

  it('opsDone', () => {
    salbp.opsDone(solution).should.deepEqual( [1, 3, 4] );
    salbp.opsDone(solution2).should.deepEqual( [1, 4] );
  });

  it('opsAvailable', () => {
    let ops = salbp.opsDone(solution);
    salbp.opsAvailable(solution, ops, 5).should.deepEqual( [] );
    salbp.opsAvailable(solution, ops, 6).should.deepEqual( [6] );
    salbp.opsAvailable(solution, ops, 7).should.deepEqual( [2, 6] );
  });

  it('stTime', () => {
    salbp.stTime(solution, 1).should.eql(3);
    salbp.stTime(solution, 2).should.eql(4);
  });

  it('activeSt', () => {
    salbp.activeSt(solution).should.eql(2);
    salbp.activeSt(solution2).should.eql(1);
  });

  describe( 'new solutions', () => {

    let checkWeights = (sol, old_sol) => {
      sol.should.have.property('weights');
      old_sol.should.have.property('weights');
      sol.weights.should.deepEqual(old_sol.weights);
    };

    let checkStations = (sol, old_sol) => {
      // base properties
      sol.should.have.property('stations');
      sol.stations.should.be.an.Array;
      old_sol.should.have.property('stations');
      old_sol.stations.should.be.an.Array;
      
      // stations
      old_sol.stations.forEach( (elm, idx) => {
        let new_s = sol[idx];
        (new_s !== undefined).should.be.true();

        new_s.should.have.property('id', elm.id);
        new_s.should.have.property('time');
        (new_s.time >= elm.time).should.be.true();
        new_s.should.have.property('operations');
        new_s.operations.should.be.an.Array;

        // operations
        elm.operations.forEach( (op, opidx) => {
          let new_op = new_s.operations[opidx];
          (new_op !== undefined).should.be.true();

          new_op.should.deepEqual(op);
        });
      });
    };

    it('newStation', () => {
      let sol = salbp.newStation(solution);

      checkWeights(sol, solution);
      checkStations(sol, solution);

      // the new station
      sol.operations.length.should.eql( solution.operations.length + 1 );
      let new_station = sol.operations[ sol.operations.length - 1 ];
      new_station.should.deepEqual({
        id: sol.operations.length,
        time: 0,
        operations: []
      });

      checkSolutionFrozen(sol);
    });

    it('assignOp (default workstation)', () => {
      let sol = salbp.assignOp(problem, solution, 2);

      checkWeights(sol, solution);
      checkStations(sol, solution);

      // the active station
      sol.stations.length.should.eql(solution.stations.length);
      sol.stations[1].time.should.eql(7);

      // the new operation
      let ops = sol.stations[1].operations,
          old_ops = solution.stations[1].operations;
      ops.length.should.eql(old_ops.length + 1);
      ops[1].should.deepEqual({ id: 2, start: 4 });

      checkSolutionFrozen(sol);
    });

    it('assignOp (requested workstation)', () => {
      let sol = salbp.assignOp(problem, solution, 2, 1);

      checkWeights(sol, solution);
      checkStations(sol, solution);

      // the first workstation
      sol.stations.length.should.eql(solution.stations.length);
      let st1 = sol.stations[0];
      st1.time.should.eql(6);
      st1.operations.length.should.eql(3);
      st1.operations[2].should.deepEqual({ id: 2, start: 3 });

      checkSolutionFrozen(sol);
    });

    it('assignOp (incorrect workstation)', () => {
      (() => assignOp(problem, solution, 2, 3)).should.throw;
    });

  });

  describe( 'solution', () => {

    it('wet', () => {
      let sol = salbp.solution(problem, 'wet', 6);

      sol.should.deepEqual({
        weights: [
          { id: 1, weight: 1 },
          { id: 2, weight: 3 },
          { id: 3, weight: 4 },
          { id: 4, weight: 2 },
          { id: 5, weight: 1 },
          { id: 6, weight: 2 },
          { id: 7, weight: 3 }
        ],

        stations: [
          { id: 1, time: 6,
            operations: [
              { id: 4, start: 0 },
              { id: 1, start: 2 },
              { id: 2, start: 3 }
            ] },
          { id: 2, time: 6,
            operations: [
              { id: 3, start: 0 },
              { id: 6, start: 4 }
            ] },
          { id: 3, time: 4,
            operations: [
              { id: 5, start: 0 },
              { id: 7, start: 1 }
            ] }
        ]
      });

      checkSolutionFrozen(sol);
    });

    it('rpw'); // do it yourself!
  });

});
